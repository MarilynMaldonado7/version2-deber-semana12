					UNIVERSIDAD TECNOLÓGICA ISRAEL
	FORMATO DE ACTIVIDADES DE SEGUIMIENTO
	INGENIERIA DE SOFTWARE
	Docente: Mg. Mario Pérez
	Estudiante: Jefferson Núñez, Alexandra Pinos, Marilyn Maldonado, Ricardo Achig.
	Fecha de entrega: 08/07/2020
Tema: Métricas.
Paralelo: 7moC.
TAREA 12
Metodología Métrica v. 3.0
La metodología Métrica Versión 3 ofrece a las Organizaciones un instrumento útil para la sistematización de las actividades que dan soporte al ciclo de vida del software.
METODOLOGIA DE MÉTRICA (PLANIFICACIÓN)
El jefe de recursos humanos tendrá disponible todas las acciones en el módulo con el software además El jefe, también, necesita reportes, por periodo, por turno y/o empleado para determinar el número de horas trabajadas y reportadas. Así también, quiere controlar el cumplimiento de las actividades encomendadas. Además, quiere verificar que hayan cumplido con su número de horas por la que se le contrató al personal poli funcional.

METODOLOGIA DE MÉTRICA (REQUISITOS)
El módulo debe enlazase a la base de datos de la empresa, MySQL, para obtener datos personales de los trabajadores poli funcionales así como el número de horas contratadas. Existe personal contratado a medio tiempo, tiempo completo y por horas.
Todos deben ingresar al módulo con usuario y contraseña.
El jefe de recursos humanos tendrá disponible todas las acciones en el módulo. Podrá registrar los turnos semanales y, distribuirlos entre el personal poli funcional. Indicará días y horas de entrada y salida, según el turno. El turno se actualiza semana a semana y depende del jefe de recursos humanos indicar cuando empieza el siguiente turno semanal, por ejemplo, de martes a lunes.

METODOLOGIA DE MÉTRICA (DISEÑO)
Debe ser creado el software en java para que todos los usuarios acceden exclusivamente a los datos del personal y de contrato.
Y tener una base de datos conectada con MySQL.

METODOLOGIA DE MÉTRICA (CODIFICACIÓN)
El módulo debe enlazase a la base de datos de la empresa, MySQL, para obtener datos personales de los trabajadores poli funcionales así como el número de horas contratadas. Existe personal contratado a medio tiempo, tiempo completo y por horas.
Todos deben ingresar al módulo con usuario y contraseña
Se solicita que el software sea desarrollado en Java, todos los usuarios utilizarán un pc que accede a la Base Datos de la empresa, exclusivamente a los datos personales y del contrato.


METODOLOGIA DE ISO APLICADO MÉTRICA (PRUEBA)
El jefe quiere asignar las responsabilidades por el módulo para que el trabajador revise lo pendiente, lo ejecute y registre su avance (% y comentario), en el módulo.

METODOLOGIA DE MÉTRICA (MANTENIMIENTO)
El software debe responder en menos de 5 segundos a cualquier transacción, debe poder recuperarse ante fallos en menos de 30 minutos Todos los campos deben ser escritos de tal forma que sean entendibles al usuario.
El software se requiere que sea fácilmente mantenible, que la tasa de error de los usuarios sea menor al 2%. Que la transacción del usuario para revisar lo pendiente, así como para registrar sus avances sea menor a 5 minutos
Un conjunto de modelos que constituya la arquitectura de información.
➢ Una propuesta de proyectos a desarrollar en los próximos años, así como la prioridad de realización de cada proyecto.
➢ Una propuesta de calendario para la ejecución de dichos proyectos.
➢ La evaluación de los recursos necesarios para los proyectos a desarrollar en el próximo año, con el objetivo de tenerlos en cuenta en los presupuestos. Para el resto de proyectos, bastará con una estimación de alto nivel.
➢ Un plan de seguimiento y cumplimiento de todo lo propuesto mediante unos mecanismos de evaluación adecuados
PSI: Planificación de sistemas de información
• Desarrollo de sistemas de información
➢ Estudio de Viabilidad del Sistema (EVS)
➢ Análisis del Sistema de Información (ASI)
➢ Diseño del Sistema de Información (DSI)
➢ Construcción del Sistema de Información (CSI)
➢ Implantación y Aceptación del Sistema (IAS)

• Mantenimiento de Sistemas de Información (MSI)
•Seguridad
El software se ejecuta solamente de forma local, y cuenta con una interfaz de usuario para el acceso, lo que impide pueda existir intrusiones no autorizadas.
• Gestión de riesgo
Mediante el modelo iterativo en el ciclo de vida del software es como se ha logrado reducir el riesgo, evaluando en cada una de las etapas del software su uso y las necesidades que se tiene con el requerimiento del cliente, para a futuro evitar que exista inconvenientes durante la aplicación del mismo.
• Mantenimiento
Los recursos necesarios para la ejecución son que tenga 2 Gb de memoria RAM, una cantidad de al menos 100 Gb para almacenar la información del sistema y un procesador de 1.5 GHz, mediante el instructivo del software se muestra cada una de las funciones que dispone a detalle.
SEGUIMIENTO
- El progreso de cada acción.
-Su efectividad en alcanzar sus objetivos.
-la evolución del proyecto en general.
- El modo de funcionar de las actividades a cargo de las funciones.
MEDICIÓN
La posibilidad de medir es un aspecto fundamental dentro del desarrollo de un proyecto; por tal motivo se debe describir con mucha precisión que es lo que sucede con el desarrollo del software.
Este procedimiento sirve en los siguientes puntos:
• Entender el proceso del desarrollo de la herramienta
• Mejorar la calidad del producto
• Tomar las mejores decisiones
Todo esto en conjunto sirve para obtener información cuantitativa en cuanto al flujo de información tanto de usuarios como de ofertantes, describir con precisión los sucesos que se presenten durante la implementación y obviamente un desarrollo apropiado para un mejor desarrollo del producto.
Para que la medición se lleve a cabo se debe tomar en cuenta:
• Desarrollar un plan de trabajo
• Ejecutar el plan de trabajo
• Evaluar las mediciones obtenidas
En conclusión, la medición facilita la toma de decisiones del grupo desarrollador del proyecto.


DE ACUERDO A LOS PUNTOS REVISADOS, INDIQUE:
La gestión si fue acertada, los desarrolladores evaluaron los riesgos pertinentes que implica el introducir un software que permita llevar la contabilidad de un negocio, manteniendo la información centralizada para tener la seguridad adecuada, y disponible. Además, que mantiene requisitos para la instalación básicos y se ha establecido un constante control de inicio a fin del desarrollo de la aplicación.
COSTOS
Costo comercial
Precio de ventas *Costo reales o de acumulacion *costo de Recuperacion
Costo de Recursos Humanos
*Desarrollador
*Encargado de las pruebas
*Enacrgado de seguimiento
Costo de Hardware *Lapto, Total Hradware
RECURSOS HUMANOS
Se refieren a las personas que ingresan, permanecen y participan en la organización, no importa cuáles sean sus niveles jerárquicos o las tareas encomendadas.

RECURSOS SOFTWARE
El software de computadora es el producto que construyen los programadores profesionales y al que después le dan mantenimiento durante un largo tiempo, incluye programas que se ejecutan en una computadora de cualquier tamaño y arquitectura, contenido que se presenta a medida que se ejecutan los programas de cómputo e información descriptiva tanto en una copia dura como en formatos virtuales que engloban virtualmente a cualesquiera medios electrónicos.
RECURSO HARDWARE
El Hardware proporciona una plataforma con las herramientas (Software) requeridas para producir los productos que son el resultado de la buena práctica de la Ingeniería del Software, un planificador de proyectos debe determinar la ventana temporal requerida para el Hardware y el Software, y verificar que estos recursos estén disponibles.
El uso de productos de código abierto y distribución gratuita, optimizan los costos del proyecto.
Otras buenas prácticas para proyectos de desarrollo de software:
1. No usar en lo posible + variables globales: puede ser modificado fácilmente
2. No utilizar sentencias goto, break y continue .
3. Usar un único return por función, que se colocará como última sentencia de la función.
4. NO usar nombres largos en las funciones.
5. No realizar copias de partes de código de otro software.
6. Las clases o módulos deben estar en ficheros separados.
7. La función main colocar en un módulo diferente al resto de código.
8. No declarar atributos de clases y registros como públicos, ni acceder a ellos directamente.
9. No ser excesivamente estrictos con la ocultación de información: podría evitar el acceso a la herencia.
Actividades Medios de verificación Supuesto Fortalecer la cultura de la empresa o institución. Informes económicos de ingresos y gastos que se generan en el desarrollo del proyecto Los recursos humanos mejoran la cultura organizacional de los asociados